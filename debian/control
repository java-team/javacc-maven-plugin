Source: javacc-maven-plugin
Section: java
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 javacc,
 jtb,
 libdoxia-java,
 libdoxia-sitetools-java,
 libmaven-invoker-plugin-java (>= 1.3),
 libmaven-plugin-tools-java,
 libmaven-reporting-api-java,
 libmaven-reporting-impl-java,
 libmaven3-core-java,
 libplexus-utils-java,
 maven-debian-helper
Standards-Version: 4.1.1
Vcs-Git: https://salsa.debian.org/java-team/javacc-maven-plugin.git
Vcs-Browser: https://salsa.debian.org/java-team/javacc-maven-plugin
Homepage: https://github.com/mojohaus/javacc-maven-plugin

Package: libjavacc-maven-plugin-java
Architecture: all
Depends: ${maven:Depends}, ${misc:Depends}
Recommends: ${maven:OptionalDepends}
Description: maven plugin which uses JavaCC to process JavaCC grammar files
 This plugin uses the JavaCC parser generator to process JavaCC grammar
 files and generate the appropriate Java source files. For general
 information about JavaCC, see the  JavaCC web site.
 .
 JavaCC grammar files use the extension .jj. This plugin can be used to
 locate and process all grammar files within a specified directory. Java
 source files will be written to a common generated-sources output
 directory, typically located in the build output directory. By default,
 the plugin will run during the generate-sources phase, but it can be
 configured to run in other phases as well.
 .
 The plugin can also be used to call the JJTree and JTB tools.
 .
 Finally, this plugin contains a goal for running JJDoc. JJDoc is a
 simple tool that generates BNF documentation for the JavaCC grammar
 files. This can be used to generate a report for your project site using
 the Maven reporting tools.
